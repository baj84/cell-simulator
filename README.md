# Cell Simulator

## Architecture
 - Webpack
 - React
 - Redux
 - Jest
 - NPM
 - ESLint
 - Babel

 While redux was not really necessary for this project I decided to implement it to demonstrate my competency using it and also allow the user to easily step back to a previous game state.

 I based my solution on two other similar implementations:
 
  - [https://github.com/charlee/react-gameoflife/](https://github.com/charlee/react-gameoflife/)
  - [https://github.com/KieranP/Game-Of-Life-Implementations/tree/master/javascript](https://github.com/KieranP/Game-Of-Life-Implementations/tree/master/javascript)

## Install

Install required node modules: `npm install` then simply `npm start`

## Testing
I've implemented just some basic testing of reducers, actions and components.

Run the tests by: `npm run test`

## Things not done
 - Webpack production config/build
 - Rule: A Cell who "comes to life" outside the board should wrap at the other side of the board. (I ran out of time).
 - Checking for homeostasis and displaying a message to the user.
 - Full test coverage

## Specification
The aim is to demonstrate how you approach thinking about problems and translating them to code.

Create a repository to your own, spend your allocated time working on a solution and then submit it back to us. Please include a README with installation and usage instructions.

Challenge: Cell Simulator
The "game" is a zero-player game, meaning that its evolution is determined by its initial state, requiring no further input. One interacts with the Cell Simulator by creating an initial configuration and observing how it evolves.

![extreme cell simulator](https://user-images.githubusercontent.com/291728/33158075-ec01ddde-d05a-11e7-99b8-35af2fed02e5.gif)

### Acceptance criteria

- At initial state, User should see an empty board.
- User can make Cells "alive".
- User can make Cells "dead".
- User can trigger "next generation".
- User can trigger a "reset" to the initial state.

### Next generation

- When the next generation is running:
  - A Cell with fewer than two live neighbours dies of under-population.
  - A Cell with 2 or 3 live neighbours lives on to the next generation.
  - A Cell with more than 3 live neighbours dies of overcrowding.
  - An empty Cell with exactly 3 live neighbours "comes to life".
  - A Cell who "comes to life" outside the board should wrap at the other side of the board.
- Once the next generation is done, User can trigger "next generation" again.

Here an example of an initial state followed by 4 "next generation":
![easy scenario](https://user-images.githubusercontent.com/7149052/53603476-bfb00e00-3c05-11e9-8862-1dfd31836dcd.jpg)

### Requirements

- Use React and TypeScript.
- Please include some attempt at testing your code.
- While not mandatory, a meaningful git history will be looked upon favourably.
