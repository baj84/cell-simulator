import React from 'react';
import PropTypes from 'prop-types';

import { CELL_SIZE } from '../constants/GameSettings';

class Cell extends React.Component {
  render() {
    const { x, y, isRunning, randomColours } = this.props;
    const hexColour = (randomColours && isRunning) ? Math.floor(Math.random()*16777215).toString(16) : '008eff';

    const styles = {
      position: 'absolute',
      backgroundColor: `#${hexColour}`,
      width: `${CELL_SIZE - 1}px`,
      height: `${CELL_SIZE - 1}px`,
      left: `${CELL_SIZE * x + 1}px`,
      top: `${CELL_SIZE * y + 1}px`
    };

    return (
      <div
        className='cell'
        style={styles}>
      </div>
    );
  }
}

Cell.propTypes = {
  isRunning: PropTypes.bool.isRequired,
  x: PropTypes.number.isRequired,
  y: PropTypes.number.isRequired
};

export default Cell;