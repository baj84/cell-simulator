import React from 'react';
import PropTypes from 'prop-types';

class Controls extends React.Component {
  render() {
    const { isRunning, onPauseClick, onStartClick, onResetClick, showResetButton, randomColours, randomColoursClick } = this.props;

    const container = {
      display: 'flex',
      justifyContent: 'space-between',
      background: '#e1e1e1',
      padding: 10,
      borderRadius: 6,
      marginBottom: 10
    };

    return (
      <div style={container}>
        <div>
          {isRunning ?
            <button className='button' onClick={onPauseClick}>Pause</button> :
            <button className='button' onClick={onStartClick}>Start</button>
          }

          {showResetButton && <button className='button' onClick={onResetClick}>Reset</button>}
        </div>
        <div>
          <input
            type='checkbox'
            value={true}
            defaultChecked={randomColours}
            onChange={randomColoursClick} /> Use random colours
        </div>
      </div>
    );
  }
}

Controls.propTypes = {
  onStartClick: PropTypes.func.isRequired,
  onPauseClick: PropTypes.func.isRequired,
  onResetClick: PropTypes.func.isRequired,
  randomColoursClick: PropTypes.func.isRequired,
  randomColours: PropTypes.bool.isRequired,
  showResetButton: PropTypes.bool
};

export default Controls;