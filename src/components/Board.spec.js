import React from 'react';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Board from './Board';
import Cell from './Cell';

Enzyme.configure({ adapter: new Adapter() });

describe('Board Component', () => {
  it('should render the correct amount of cells', () => {
    const cells = [{x: 0, y: 0}, {x: 1, y: 1}];

    const wrapper = mount(<Board cells={cells} isRunning={true} onCellClick={() => {}} />);

    expect(wrapper.find('.cell')).toBeDefined();
    expect(wrapper.find('.cell')).toHaveLength(cells.length);
  });
});
