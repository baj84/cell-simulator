import React from 'react';

import GameContainer from '../Containers/Game';

class App extends React.Component {
  render() {
    const styles = {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '100%'
    };

    return (
      <div style={styles}>
        <GameContainer />
      </div>
    );
  }
}

export default App;