import React from 'react';
import PropTypes from 'prop-types';

import { CELL_SIZE, WIDTH, HEIGHT, ROWS, COLS } from '../constants/GameSettings';
import Cell from './Cell';

class Board extends React.Component {
  getElementOffset() {
    const rect = this.boardRef.getBoundingClientRect();
    const doc = document.documentElement;
    return {
      x: (rect.left + window.pageXOffset) - doc.clientLeft,
      y: (rect.top + window.pageYOffset) - doc.clientTop,
    };
  }

  handleClick(event) {
    const elemOffset = this.getElementOffset();
    const offsetX = event.clientX - elemOffset.x;
    const offsetY = event.clientY - elemOffset.y;
    
    const x = Math.floor(offsetX / CELL_SIZE);
    const y = Math.floor(offsetY / CELL_SIZE);

    if (x >= 0 && x <= COLS && y >= 0 && y <= ROWS) {
      this.props.onCellClick({y, x});
    }
  }

  render() {
    const styles = {
      position: 'relative',
      margin: '0 auto',
      backgroundColor: '#0b75d8',
      backgroundImage: 'linear-gradient(#333 1px, transparent 1px), linear-gradient(90deg, #333 1px, transparent 1px)',
      backgroundSize: `${CELL_SIZE}px ${CELL_SIZE}px`,
      width: `${WIDTH}px`,
      height: `${HEIGHT}px`
    };

    return (
      <div>
        <div 
          style={styles}
          onClick={this.handleClick.bind(this)}
          ref={(n) => { this.boardRef = n; }}>
          {this.props.cells.map(cell => (
            <Cell
              isRunning={this.props.isRunning}
              randomColours={this.props.randomColours}
              x={cell.x}
              y={cell.y}
              key={`${cell.x},${cell.y}`} />
          ))}
        </div>
      </div>
    );
  }
}

Board.propTypes = {
  cells: PropTypes.array.isRequired,
  onCellClick: PropTypes.func.isRequired,
  isRunning: PropTypes.bool.isRequired,
  randomColours: PropTypes.bool
};

export default Board;