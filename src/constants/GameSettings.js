export const CELL_SIZE = 20;
export const WIDTH = 600;
export const HEIGHT = 400;
export const ROWS = HEIGHT / CELL_SIZE;
export const COLS = WIDTH / CELL_SIZE;
export const INTERVAL = 100;

export default {
  CELL_SIZE,
  WIDTH,
  HEIGHT,
  ROWS,
  COLS,
  INTERVAL
};