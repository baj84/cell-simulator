import * as actionTypes from '../constants/actionTypes';
import reducer from './gameReducer';

describe('Game Reducer', () => {
  const getInitialState = () => {
    return {
      randomColours: false,
      firstRun: true,
      isRunning: false,
      board: [],
      cells: [],
      firstBoardState: []
    };
  };

  const getAppState = () => {
    return {
      randomColours: false,
      firstRun: true,
      isRunning: false,
      board: [
        [ false, true, false ],
        [ false, false, false ],
        [ false, false, false ]
      ],
      cells: [
        { y: 0, x: 1 },
        { y: 1, x: 1 }
      ],
      firstBoardState: []
    };
  };

  it('CREATE_NEW_GAME: Should assign board and cells', () => {
    const cells = [
      { y: 0, x: 1 },
      { y: 1, x: 1 },
      { y: 2, x: 1 }
    ];

    const board = [
      [ false, false, false ],
      [ false, false, false ],
      [ false, false, false ],
      [ false, true, false ]
    ];

    const action = { type: actionTypes.CREATE_NEW_GAME, payload: { board, cells } };
    const expected = Object.assign(getAppState(), { board, cells });

    expect(reducer(getAppState(), action)).toEqual(expected);
  });

  it('TOGGLE_CELL: Should mark cell as alive', () => {
    const coords = { y: 2, x: 2 };

    const action = { type: actionTypes.TOGGLE_CELL, payload: coords };
    const state = reducer(getAppState(), action);

    expect(state.board[2][2]).toBe(true);
  });

  it('TOGGLE_CELL: Should mark cell as dead', () => {
    const coords = { y: 0, x: 1 };

    const action = { type: actionTypes.TOGGLE_CELL, payload: coords };
    const state = reducer(getAppState(), action);

    expect(state.board[0][1]).toBe(false);
  });
});
