import { createReducer, clone } from '../helpers/redux';

import * as actionTypes from '../constants/actionTypes'; 

const initialState = {
  randomColours: false,
  firstRun: true,
  isRunning: false,
  board: [],
  cells: [],
  firstBoardState: []
};

export default createReducer(initialState, {
  [actionTypes.CREATE_NEW_GAME]: (state, payload) => {
    return {
      ...state,
      firstRun: true,
      isRunning: false,
      firstBoardState: [],
      cells: payload.cells,
      board: payload.board
    };
  },
  [actionTypes.RENDER_BOARD]: (state, payload) => {
    return {
      ...state,
      cells: payload.cells,
      board: payload.board
    };
  },
  [actionTypes.TOGGLE_CELL]: (state, coords) => {
    const { y, x } = coords;

    let newBoard = clone(state.board);
    let newCells = clone(state.cells);

    // Toggle the cell state
    newBoard[y][x] = !newBoard[y][x];

    // Add active cell to our cell list
    if (newBoard[y][x]) {
      newCells.push({ x, y });
    } else {
      // Remove the cell from our list
      newCells = newCells.filter(item => !(item.x === x && item.y === y));
    }

    return {
      ...state,
      board: newBoard,
      cells: newCells
    };
  },
  [actionTypes.SET_GAME_RUNNING]: (state) => {
    return {
      ...state,
      isRunning: true,
      firstBoardState: state.firstRun ? clone(state.board) : state.firstBoardState,
      firstRun: false
    };
  },
  [actionTypes.PAUSE_GAME]: (state) => {
    return {
      ...state,
      isRunning: false
    };
  },
  [actionTypes.TOGGLE_RANDOM_COLOURS]: (state) => {
    return {
      ...state,
      randomColours: !state.randomColours
    };
  }
});
