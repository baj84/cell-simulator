import * as actionTypes from '../constants/actionTypes';
import * as actionCreators from './gameActions';

describe('Game Actions', () => {
  const appState = {
    randomColours: false,
    firstRun: true,
    isRunning: false,
    board: [
      [ false, true, false ],
      [ false, false, false ],
      [ false, false, false ]
    ],
    cells: [
      { y: 0, x: 1 },
      { y: 1, x: 1 }
    ],
    firstBoardState: []
  };

  it('start() should create an action to start the game', () => {
    const dispatch = jest.fn();
    const expected = {
      type: actionTypes.SET_GAME_RUNNING
    };

    const getState = () => { return { game: appState } };

    expect(typeof(actionCreators.start(appState))).toEqual('function');

    actionCreators.start(appState)(dispatch, getState);

    expect(dispatch).toBeCalledWith(expected);
  });

  it('start() should not start the game and call an alert', () => {
    jest.spyOn(window, 'alert').mockImplementation(() => {});

    const dispatch = jest.fn();
    const expected = {
      type: actionTypes.SET_GAME_RUNNING
    };

    const getState = () => {
      return {
        game: {
          ...appState,
          cells: []
        }
      }
    };

    actionCreators.start(appState)(dispatch, getState);

    expect(dispatch).not.toHaveBeenCalled();
    expect(window.alert).toHaveBeenCalledTimes(1);
  });

  describe('calculateNeighbours', () => {
    it('should return 3 neighbours', () => {
      const board = [
        [ false, true, false ],
        [ false, true, true ],
        [ false, true, false ]
      ];
      const y = 1;
      const x = 1;
      const rows = 3
      const cols = 3;

      const result = actionCreators.calculateNeighbours(board, x, y, rows, cols);

      expect(result).toEqual(3);
    });

    it('should return a single neighbour', () => {
      const board = [
        [ false, false, false ],
        [ false, true, false ],
        [ false, true, false ]
      ];
      const y = 1;
      const x = 1;
      const rows = 3
      const cols = 3;

      const result = actionCreators.calculateNeighbours(board, x, y, rows, cols);

      expect(result).toEqual(1);
    });


    it('should return no neighbours', () => {
      const board = [
        [ false, false, false ],
        [ false, true, false ],
        [ false, false, false ],
        [ true, false, true ]
      ];
      const y = 1;
      const x = 1;
      const rows = 4
      const cols = 4;

      const result = actionCreators.calculateNeighbours(board, x, y, rows, cols);

      expect(result).toEqual(0);
    });
  });

  describe('applyGameRules', () => {
    beforeAll(() => {
      const emptyBoard = [
        [ false, false, false ],
        [ false, false, false ],
        [ false, false, false ]
      ];

      const mock = jest.spyOn(actionCreators, 'makeBoard');
      mock.mockImplementation(() => emptyBoard);
    });

    it('A Cell with 2 or 3 live neighbours lives on to the next generation.', () => {
      const board = [
        [ true, true, false ],
        [ false, true, false ],
        [ false, false, false ]
      ];
      const rows = 3
      const cols = 3;

      const result = actionCreators.applyGameRules(board, rows, cols);

      expect(result[0][1]).toBe(true);
    });

    it('A Cell with fewer than two live neighbours dies of under-population.', () => {
      const board = [
        [ false, true, false ],
        [ false, true, false ],
        [ false, false, false ]
      ];
      const rows = 3
      const cols = 3;

      const result = actionCreators.applyGameRules(board, rows, cols);

      expect(result[0][1]).toBe(false);
      expect(result[1][1]).toBe(false);
    });

    it('A Cell with more than 3 live neighbours dies of overcrowding.', () => {
      const board = [
        [ true, true, true ],
        [ true, true, false ],
        [ false, false, false ]
      ];
      const rows = 3
      const cols = 3;

      const result = actionCreators.applyGameRules(board, rows, cols);

      expect(result[0][1]).toBe(false);
    });

    it('An empty Cell with exactly 3 live neighbours "comes to life"', () => {
      const board = [
        [ true, false, true ],
        [ false, true, false ],
        [ false, false, false ]
      ];
      const rows = 3
      const cols = 3;

      const result = actionCreators.applyGameRules(board, rows, cols);

      expect(result[0][1]).toBe(true);
    });
  });
});
