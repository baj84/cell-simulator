import * as actionTypes from '../constants/actionTypes';
import { createAction} from '../helpers/redux';

import { ROWS, COLS, INTERVAL } from '../constants/GameSettings';

// The game timer
let timeoutInt = null;

/**
 * Creates a matrix to represent our board
 *
 * @returns {Array}
 */
export function makeBoard() {
  let board = [];

  for (let y = 0; y < ROWS; y++) {
    board[y] = [];

    for (let x = 0; x < COLS; x++) {
      // Set all initial cells to dead
      board[y][x] = false;
    }
  }

  return board;
}

/**
 * Stores the coordinates for all alive cells
 *
 * @param {Array} board
 * @returns {Array}
 */
export function makeCells(board) {
  let cells = [];

  for (let y = 0; y < ROWS; y++) {
    for (let x = 0; x < COLS; x++) {
      // If a cell is alive store it
      if (board[y][x]) {
        cells.push({ x, y });
      }
    }
  }

  return cells;
}

/**
 * Calculates the number of neighbours for a given cell
 *
 * @param {Array} board
 * @param {int} x
 * @param {int} y
 * @param {int} rows
 * @param {int} cols
 * @returns {int}
 */
export function calculateNeighbours(board, x, y, rows, cols) {
  let neighbours = 0;
  // All possible directions
  const dirs = [[-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1]];

  for (let i = 0; i < dirs.length; i++) {
    const dir = dirs[i];
    let y1 = y + dir[0];
    let x1 = x + dir[1];

    // Flag as a neighbour if the cell is within the board and it's alive
    if (x1 >= 0 && x1 < cols && y1 >= 0 && y1 < rows && board[y1][x1]) {
      neighbours++;
    }
  }

  return neighbours;
}

/**
 * Applies the game rules to the board and returns
 * a new board with the updated cell states
 *
 * @param {Array} board
 * @param {int} rows
 * @param {int} cols
 * @returns {Array}
 */
export function applyGameRules(board, rows, cols) {
  const newBoard = makeBoard();

  for (let y = 0; y < rows; y++) {
    for (let x = 0; x < cols; x++) {
      let neighbours = calculateNeighbours(board, x, y, rows, cols);
      if (board[y][x]) {
        // A Cell with 2 or 3 live neighbours lives on to the next generation.
        if (neighbours === 2 || neighbours === 3) {
          newBoard[y][x] = true;
        } else {
          // A Cell with fewer than two live neighbours dies of under-population.
          // A Cell with more than 3 live neighbours dies of overcrowding.
          newBoard[y][x] = false;
        }
      } else {
        // An empty Cell with exactly 3 live neighbours "comes to life".
        if (!board[y][x] && neighbours === 3) {
          newBoard[y][x] = true;
        }
      }
    }
  }

  return newBoard;
}

/**
 * Run the next iteration of the game
 *
 * @returns {Function}
 */
export function runNextGeneration() {
  return function (dispatch, getState) {
    const board = getState().game.board;

    // Get the updated board
    const newBoard = applyGameRules(board, ROWS, COLS);
    const cells = makeCells(newBoard);

    dispatch(createAction(actionTypes.RENDER_BOARD, { board: newBoard, cells }));

    // TODO: Check if the cells are the same as previous state
    // and if so pause the game and alert the user that the game
    // has reached homeostasis

    if (!cells.length) {
      dispatch(pause());

      alert('Game Over! All the cells are dead.');
    } else {
      timeoutInt = window.setTimeout(() => {
        if (timeoutInt) {
          dispatch(runNextGeneration());        
        }
      }, INTERVAL);
    }
  };
}

/**
 * Create a new game with an empty board
 *
 * @returns {Function}
 */
export function newGame() {
  return function (dispatch) {
    const board = makeBoard();
    const cells = makeCells(board);

    dispatch(createAction(actionTypes.CREATE_NEW_GAME, { board, cells }));
  };
}

/**
 * Create a new game using the initial state first selected by the user
 *
 * @returns {Function}
 */
export function reset() {
  return function (dispatch, getState) {
    dispatch(pause());

    const board = getState().game.firstBoardState;
    const cells = makeCells(board);

    dispatch(createAction(actionTypes.CREATE_NEW_GAME, { board, cells }));
  };
}

/**
 * Flags a cell as alive or dead
 *
 * @returns {Function}
 */
export function toggleCell(coords) {
  return function (dispatch) {
    dispatch(createAction(actionTypes.TOGGLE_CELL, coords));
  };
}

/**
 * Starts the game timer off
 *
 * @returns {Function}
 */
export function start() {
  return function (dispatch, getState) {
    const cells = getState().game.cells;

    if (!cells.length) {
      window.alert('Please select an initial state by clicking on some cells.');
    } else {
      dispatch(createAction(actionTypes.SET_GAME_RUNNING));

      dispatch(runNextGeneration());
    }
  };
}

/**
 * Pauses the game by clearing the timer
 *
 * @returns {Function}
 */
export function pause() {
  return function (dispatch, getState) {
    dispatch(createAction(actionTypes.PAUSE_GAME));

    if (timeoutInt) {
      window.clearTimeout(timeoutInt);
      timeoutInt = null;
    }
  };
}

/**
 * Toggles random colours
 *
 * @returns {Function}
 */
export function toggleRandomColours() {
  return function (dispatch, getState) {
    dispatch(createAction(actionTypes.TOGGLE_RANDOM_COLOURS));
  };
}
