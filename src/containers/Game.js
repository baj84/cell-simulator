import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Board from '../components/Board';
import Controls from '../components/Controls';

import * as actions from '../actions/gameActions';

class Game extends React.Component {
  componentDidMount() {
    this.props.actions.newGame();
  }

  render() {
    const showResetButton = !!(!this.props.firstRun && this.props.hasFirstState);

    return (
      <div>
        <Controls
          isRunning={this.props.isRunning}
          onStartClick={this.props.actions.start}
          onPauseClick={this.props.actions.pause}
          onResetClick={this.props.actions.reset}
          showResetButton={showResetButton}
          randomColours={this.props.randomColours}
          randomColoursClick={this.props.actions.toggleRandomColours}
        />

        <Board
          isRunning={this.props.isRunning}
          cells={this.props.cells}
          onCellClick={this.props.actions.toggleCell}
          randomColours={this.props.randomColours}
        />
      </div>
    );
  }
}

Game.propTypes = {
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    isRunning: state.game.isRunning,
    randomColours: state.game.randomColours,
    firstRun: state.game.firstRun,
    hasFirstState: state.game.firstBoardState.length,
    board: state.game.board,
    cells: state.game.cells
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Game);
